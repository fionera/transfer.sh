package main

import "git.kiska.pw/kiska/transfer.sh/cmd"

func main() {
	app := cmd.New()
	app.RunAndExitOnError()
}
